My name is Frank van Es. I live in a city in The Netherlands, called Gouda, together with my girlfriend Eefje, my son Jurriaan, and my daughter Stella.

I currently work as IT Architect at a Dutch company called Logius, which is the digital government service of the Netherlands Ministry of the Interior and Kingdom Relations.

I am a strong advocate of Agile practices and my interests and strenghts mainly focus around lightweight architectures through the use of Web APIs, front-end technologies, microservices and containerization.
In my spare time I love travelling, (mainly travel-) photography, spending time with my family and friends and definitely some good food, beers, and wines.
